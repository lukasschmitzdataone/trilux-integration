package org.example.Trilux;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

@RestController
public class ControllerToSF {
    private boolean integrationtoSFrunning = false;
    private String currentId = "";
    private String urlToAV;
    private String authorizationToAV;
    private String urlToSF;
    private String authorizationToSF;

    private static final Environment ENVIRONMENT = Environment.DEVELOPMENT;
    private static final JobPeriod JOB_PERIOD = JobPeriod.DAILY;

    private static final String AV_DEV_URL = "https://txavendoo.trilux.com/elearning/api/v1/achievement/getAchievements/usergroup/1362409035549_1/datalimit/";
    private static final String AV_DEV_AUTH = "Basic YXBpU2FwU2Y6XyFuWjltZTQ0P1dJ";
    private static final String AV_PRD_URL = ""; // todo add prod url
    private static final String AV_PRD_AUTH = ""; // todo add prod auth

    private static final String DATA_LIMIT = "10";
    private static final String SF_DEV_URL = "https://api12preview.sapsf.eu/odata/v2/upsert";
    private static final String SF_DEV_AUTH = "Basic QVBJX0F2ZW5kb29AVHJpbHV4VDpXZWxjb21lMSE=";
    private static final String SF_PRD_URL = ""; // todo add prod url
    private static final String SF_PRD_AUTH = ""; // todo add prod auth


    @GetMapping("/startIntegrationToSF/{startId}")
    public ResponseEntity<String> startIntegrationToSF(@PathVariable("startId") String startId){

        if (integrationtoSFrunning){
            return new ResponseEntity<>("Integration to SF is currently running. Please call /stopIntegrationToSF first.", HttpStatus.BAD_REQUEST);
        }

        System.out.println("Starting Integration to SF with Id " + startId);

        integrationtoSFrunning = true;
        currentId = startId;
        String achievementData = "";
        String postResult = "";

        switch (ENVIRONMENT) {
            case DEVELOPMENT:
            case STAGING:
                urlToAV = AV_DEV_URL + DATA_LIMIT;
                authorizationToAV = AV_DEV_AUTH;
                urlToSF = SF_DEV_URL;
                authorizationToSF = SF_DEV_AUTH;
                break;
            case PRODUCTION:
                urlToAV = AV_PRD_URL + DATA_LIMIT;
                authorizationToAV = AV_PRD_AUTH;
                urlToSF = SF_PRD_URL;
                authorizationToSF = SF_PRD_AUTH;
                break;
        }

        while (integrationtoSFrunning){
            achievementData = performGETRequestFromAV();

            if (achievementData.length() != 0){
                JSONArray achievementArr = new JSONArray(achievementData);

                //System.out.println("Achievement Array: " + achievementArr.toString()); // todo remove

                for (int i = 0; i < achievementArr.length(); i++){
                    JSONObject achievement = achievementArr.getJSONObject(i);
                    currentId = achievement.getNumber("id").toString();

                    postResult = performPOSTRequestToSF(transformAchievement(achievement));
                }

                integrationtoSFrunning = false; //todo remove for prod
            } else{

                try {
                    TimeUnit.MINUTES.sleep(JOB_PERIOD.getMinutes());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                integrationtoSFrunning = false; //todo remove for prod
            }
        }

        return new ResponseEntity<>(achievementData, HttpStatus.OK);
    }

    @GetMapping("/stopIntegrationToSF")
    public ResponseEntity<String> stopIntegrationToSF(){
        integrationtoSFrunning = false;

        System.out.println("Stopping Integration to SF with Id " + currentId);
        return new ResponseEntity<>("Successfully stopped Integration to SF", HttpStatus.OK);
    }


    private String performGETRequestFromAV(){
        String achievementData = "";
        try {
            URL url = new URL(urlToAV);
            HttpURLConnection con = null;
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            con.setRequestProperty("Authorization", authorizationToAV);
            con.setRequestProperty("Content-Type", "application/xml");
            con.setRequestProperty("startTime", currentId); //insert id here (1606215892676)
            con.setRequestProperty("Accept", "application/xml");

            con.setReadTimeout(10000);
            con.setConnectTimeout(15000);

            int status = con.getResponseCode();

            if (status == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuilder content = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }

                in.close();

                if (content.length() != 0) {
                    String xml = content.toString();
                    xml = xml.replaceAll("dooacs:", "");
                    JSONObject json = XML.toJSONObject(xml);
                    json = (JSONObject) json.get("achievements");

                    if (json.has("achievement")) {
                        JSONArray achievementArr;
                        Object achievementObj = json.get("achievement");

                        if (achievementObj instanceof JSONArray) {
                            achievementArr = (JSONArray) achievementObj;
                        } else{
                            achievementArr = new JSONArray();
                            achievementArr.put((JSONObject) achievementObj);
                        }

                        achievementData = achievementArr.toString();
                    }
                }
            } else {
                System.out.println("error " + status);
            }

            con.disconnect();

        } catch (Exception e) {
            System.out.println("Error when running integration. Last processed element: " + currentId);
            e.printStackTrace();
        }

        return achievementData;
    }

    private String performPOSTRequestToSF(JSONObject custAvendooParent){
        try {
            URL url = new URL(urlToSF);
            HttpURLConnection con = null;
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");

            con.setRequestProperty("Authorization", authorizationToSF);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Accept-Charset", "UTF-8");

            con.setReadTimeout(10000);
            con.setConnectTimeout(15000);
            con.setDoOutput(true);

            try(OutputStreamWriter  os = new OutputStreamWriter(con.getOutputStream())) {
                os.write(custAvendooParent.toString());
                os.flush();

                System.out.println("POST Body: " + custAvendooParent.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            int status = con.getResponseCode();

            if (status == 201){

            } else {

                System.out.println("Error " + status + ": " + con.getResponseMessage());
            }

            con.disconnect();


        } catch (Exception e) {
            System.out.println("Error when running integration. Last processed element: " + currentId);
            e.printStackTrace();
        }

        return "";
    }

    private JSONObject transformAchievement(JSONObject achievementAV) {
        String userLogin = achievementAV.optString("userLogin").replaceAll("^0+(?!$)", "");
        Number startDateTS = achievementAV.getNumber("startDate");
        String startDate = "/Date(" + startDateTS.toString() + ")/";
        String startDateTime = "/Date(" + startDateTS.toString() + "+0200)/";
        String endDate = "/Date(" + achievementAV.getNumber("endDate").toString() + ")/";
        String objectTypeName =  achievementAV.optString("objectTypeName");
        String effectiveStartDate = objectTypeName.equals("CERTIFICATION") ? startDate : endDate;

        JSONObject achievementSF = new JSONObject();
        achievementSF.put("externalCode", userLogin + "_" + achievementAV.optString("seminarId"));
        achievementSF.put("cust_Avendoo_Parent_externalCode", userLogin);
        achievementSF.put("cust_Avendoo_Parent_effectiveStartDate", effectiveStartDate);
        achievementSF.put("cust_ObjectTypeName", objectTypeName);
        achievementSF.put("cust_StartDate", startDateTime);
        achievementSF.put("cust_EndDate", endDate);
        achievementSF.put("cust_UserLogin", achievementAV.optString("userLogin"));
        achievementSF.put("cust_Title", achievementAV.optString("title"));
        achievementSF.put("cust_Subject", achievementAV.optString("subject"));
        achievementSF.put("cust_Provider", achievementAV.optString("provider"));
        achievementSF.put("cust_Location", achievementAV.optString("location"));

        int duration = achievementAV.getInt("duration") / 1000 / 60;
        if(duration > 0) {
            achievementSF.put("cust_Duration", duration / 60 + ":" + duration % 60);
        }

        JSONArray custToAvendooChild = new JSONArray();
        custToAvendooChild.put(achievementSF);

        JSONObject metaData = new JSONObject();
        String custAvendooParenUri = "https://api12preview.sapsf.eu/odata/v2/cust_Avendoo_Parent"
                + "(effectiveStartDate=datetime'%1$s',externalCode='%2$s')";
        String effectiveStartDateMetaData = LocalDateTime.ofInstant(Instant.ofEpochMilli(startDateTS.longValue()),
                ZoneId.of( "Europe/Paris" )).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T00:00'"));
        metaData.put("uri", String.format(custAvendooParenUri, effectiveStartDateMetaData, userLogin));

        JSONObject custAvendooParent = new JSONObject();
        custAvendooParent.put("cust_toAvendooChild", custToAvendooChild);
        custAvendooParent.put("effectiveStartDate", effectiveStartDate);
        custAvendooParent.put("externalCode", userLogin);
        custAvendooParent.put("__metadata", metaData);

        return custAvendooParent;
    }
}