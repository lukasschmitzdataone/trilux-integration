package org.example.Trilux;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class ControllerToAV {
    private boolean integrationtoAVrunning = false;
    private String urlToSF;
    private String authorizationToSF;
    private String urlToAV;
    private String authorizationToAV;

    private static final Environment ENVIRONMENT = Environment.DEVELOPMENT;
    private static final JobPeriod jobPeriod = JobPeriod.DAILY;

    private static final String SF_DEV_URL = "https://api12preview.sapsf.eu/odata/v2/PerPerson";
    private static final String SF_DEV_AUTH = "Basic QVBJX0F2ZW5kb29AVHJpbHV4VDpITVFDSWRyWTM3Z3dBZVRDNE0=";
    private static final String SF_PRD_URL = ""; // todo add prod url
    private static final String SF_PRD_AUTH = ""; // todo add prod auth

    private static final String AV_DEV_URL = "https://lernwelt.avendoo.de/api/v1/user/stringImport";
    private static final String AV_DEV_AUTH = "Basic aW1wb3J0Ol5Ic2Q5MVYiaSVWMA==";
    private static final String AV_PRD_URL = ""; // todo add prod url
    private static final String AV_PRD_AUTH = ""; // todo add prod auth

    private static final String USER_MASTER_DATA_HEADER = "login;vorna;nachn;email;title;gender;lang;oe;company;street;" +
            "postcode;location;group;division;phone;mobile_phone;mobile_phone_private;field10;" +
            "bsgrd;field12;field13;field14;personnel_number;expiry_date;entry_date;superior_login;" +
            "trainer_login;region;mobile_phone_operator;description;role;billing_company;" +
            "billing_firstname;billing_lastname;billing_street;billing_postcode;" +
            "billing_location;billing_email;billing_country;address_is_billing_address;" +
            "extern;cost_center;hr_login;training_discount;country";

    @GetMapping("/startIntegrationToAV")
    public ResponseEntity<String> startIntegrationToAV() {

        if (integrationtoAVrunning) {
            return new ResponseEntity<>("Integration to AV is currently running. Please call /stopIntegrationToAV first.", HttpStatus.BAD_REQUEST);
        }

        System.out.println("Starting Integration to AV");

        integrationtoAVrunning = true;
        String userMasterData = "";
        String postResult = "";

        switch (ENVIRONMENT) {
            case DEVELOPMENT:
            case STAGING:
                urlToSF = SF_DEV_URL;
                authorizationToSF = SF_DEV_AUTH;
                urlToAV = AV_DEV_URL;
                authorizationToAV = AV_DEV_AUTH;
                break;
            case PRODUCTION:
                urlToSF = SF_PRD_URL;
                authorizationToSF = SF_PRD_AUTH;
                urlToAV = AV_PRD_URL;
                authorizationToAV = AV_PRD_AUTH;
                break;
        }

        while (integrationtoAVrunning) {
            userMasterData = performGETRequestFromSF();

            if (userMasterData.length() != 0) {
                postResult = performPOSTRequestToAV(transfomrUserMasterData(userMasterData));

                integrationtoAVrunning = false; //todo remove for prod
            } else {
                try {
                    TimeUnit.MINUTES.sleep(jobPeriod.getMinutes());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                integrationtoAVrunning = false; //todo remove
            }
        }

        return new ResponseEntity<>(userMasterData, HttpStatus.OK);
    }

    @GetMapping("/stopIntegrationToAV")
    public ResponseEntity<String> stopIntegrationToAV() {
        integrationtoAVrunning = false;

        System.out.println("Stopping Integration to AV");
        return new ResponseEntity<>("Successfully stopped Integration to AV", HttpStatus.OK);

    }

    private String performGETRequestFromSF() {
        String userMasterData = "";
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("format", "JSON");
            //params.put("top", "1"); // todo dynamic approach
            //params.put("skip", "0"); // todo dynamic approach
            params.put("filter", "personIdExternal eq '12883'"); // todo remove
            params.put("expand", "userAccountNav,"
                        + "personalInfoNav,"
                        + "personalInfoNav/nativePreferredLangNav,"
                        + "emailNav,"
                        + "employmentNav,"
                        + "employmentNav/jobInfoNav,"
                        + "employmentNav/jobInfoNav/locationNav/addressNavDEFLT");
            params.put("select", "userAccountNav/username,"
                        + "personalInfoNav/firstName,"
                        + "personalInfoNav/lastName,"
                        + "personalInfoNav/title,"
                        + "personalInfoNav/gender,"
                        + "personalInfoNav/personIdExternal,"
                        + "personalInfoNav/nativePreferredLangNav/externalCode,"
                        + "emailNav/emailAddress,"
                        + "employmentNav/startDate,"
                        + "employmentNav/jobInfoNav/company,"
                        + "employmentNav/jobInfoNav/jobCode,"
                        + "employmentNav/jobInfoNav/fte,"
                        + "employmentNav/jobInfoNav/managerId,"
                        + "employmentNav/jobInfoNav/payGroup,"
                        + "employmentNav/jobInfoNav/costCenter,"
                        + "employmentNav/jobInfoNav/countryOfCompany,"
                        + "employmentNav/jobInfoNav/locationNav/addressNavDEFLT/zipCode,"
                        + "employmentNav/jobInfoNav/locationNav/addressNavDEFLT/city,"
                        + "employmentNav/jobInfoNav/locationNav/addressNavDEFLT/address1");

            URL url = new URL(urlToSF + getParamsString(params));
            HttpURLConnection con = null;
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            con.setRequestProperty("Authorization", authorizationToSF);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Accept-Charset", "UTF-8");

            con.setReadTimeout(10000);
            con.setConnectTimeout(15000);

            int status = con.getResponseCode();

            if (status == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuilder content = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }

                in.close();

                if (content.length() != 0) {
                    JSONObject json = new JSONObject(content.toString());
                    json = (JSONObject) json.get("d");
                    JSONArray userMasterDataArr = json.getJSONArray("results");
                    userMasterData = userMasterDataArr.toString();
                }
            } else {
                System.out.println("error " + status);
            }

            con.disconnect();

        } catch (Exception e) {
            System.out.println("Error when running integration.");
            e.printStackTrace();
        }

        return userMasterData;
    }

    private String performPOSTRequestToAV(String userMasterData) {
        if (1 == 1) return ""; // todo remove after master record is build

        try {
            URL url = new URL(urlToAV);
            HttpURLConnection con = null;
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");

            con.setRequestProperty("Content-Type", "text/plain");
            con.setRequestProperty("field0Builder", "field5;'-';superior_login");
            con.setRequestProperty("Accept", "application/xml");
            con.setRequestProperty("Authorization", authorizationToAV);
            con.setRequestProperty("standardPermissionGroup", "8722523670654956890");
            con.setRequestProperty("passwordgenerationoption", "2");
            con.setRequestProperty("minrows", "0");
            con.setRequestProperty("userimportfield9value", "importpw");
            con.setRequestProperty("changedCsvHeader", "field11=bsgrd;lastname=nachn;firstname=vorna;field4=location;field3=postcode;costCenter=cost_center;field6=division;field5=oe;field1=company;field0=superior_login;field7=phone;superiorLogin=superior_login;field2=street;preferredLanguage=lang;field8=mobile_phone;personnelNumber=personnel_number");
            con.setRequestProperty("userImportCsvSeparator", ";");
            con.setRequestProperty("userImportCsvCharEncoding", "UTF-8");
            con.setRequestProperty("loginmechanismus", "0");
            con.setRequestProperty("changepasswordafterlogin", "false");
            con.setRequestProperty("userimportdeleteoption", "1");
            con.setRequestProperty("sendpasswordpermail", "false");
            con.setRequestProperty("userGroup", "1615361176152_1");
            con.setRequestProperty("userImportIgnoreForUpdateFieldsOption", "description");
            con.setRequestProperty("userImportPermanentDeleteOption", "0");

            //String body = "login;vorna;nachn;email;title;gender;lang;oe;company;street;postcode;location;group;division;phone;mobile_phone;mobile_phone_private;field10;bsgrd;field12;field13;field14;personnel_number;expiry_date;entry_date;superior_login;trainer_login;region;mobile_phone_operator;description;role;billing_company;billing_firstname;billing_lastname;billing_street;billing_postcode;billing_location;billing_email;billing_country;address_is_billing_address;extern;cost_center;hr_login;training_discount;country\n" +
            //        "tloke-importtest;Tobias;Loke;tobias.loke@magh-boppert.de;;m;de;MB;Magh und Boppert GmbH;Schulze-Delitzsch-Straße 8;33100;Paderborn;0000022200;71000391;;;;;100.00;;;;00000003;;23.02.1981;00000700;;NRW/Arnsberg;;;;;;;;;;;;;0;0000022200;;;Deutschland";


            con.setDoOutput(true);
            try (OutputStream os = con.getOutputStream()) {
                os.write(userMasterData.getBytes());
            }

            int status = con.getResponseCode();

            if (status == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                in.close();

                String xml = content.toString();
                return xml;
            } else {
                System.out.println("error " + status);
            }

            con.disconnect();


        } catch (Exception e) {
            System.out.println("Error when running integration.");
            e.printStackTrace();
        }
        return "";

    }

    private String transfomrUserMasterData(String userMasterDataSF) {
        String userMasterDataAV = USER_MASTER_DATA_HEADER;
        JSONArray userMasterDataArr = new JSONArray(userMasterDataSF);

        for (int i = 0; i < userMasterDataArr.length(); i++) {
            JSONObject userMasterDataEntry = userMasterDataArr.getJSONObject(i);
            JSONObject userAccountNav = new JSONObject();
            JSONObject personalInfoNav = new JSONObject();
            JSONObject nativePreferredLangNav = new JSONObject();
            JSONObject emailNav = new JSONObject();
            JSONObject employmentNav = new JSONObject();
            JSONObject jobInfoNav = new JSONObject();
            JSONObject locationNav = new JSONObject();
            JSONObject addressNavDEFLT = new JSONObject();
            JSONArray tmpArray;

            System.out.println("ID" + i + ": " + userMasterDataEntry.toString() + "\n");

            if (userMasterDataEntry.has("userAccountNav")) {
                userAccountNav = userMasterDataEntry.getJSONObject("userAccountNav");
            }

            if (userMasterDataEntry.has("personalInfoNav")) {
                personalInfoNav = userMasterDataEntry.getJSONObject("personalInfoNav");
                tmpArray = personalInfoNav.getJSONArray("results");
                personalInfoNav = tmpArray.getJSONObject(0);

                if (personalInfoNav.has("nativePreferredLangNav")) {
                    nativePreferredLangNav = personalInfoNav.getJSONObject("nativePreferredLangNav");
                }
            }

            if (userMasterDataEntry.has("emailNav")) { // todo restrict to business address
                emailNav = userMasterDataEntry.getJSONObject("emailNav");
                tmpArray = emailNav.getJSONArray("results");
                emailNav = tmpArray.getJSONObject(0);
            }

            if (userMasterDataEntry.has("employmentNav")) {
                employmentNav = userMasterDataEntry.getJSONObject("employmentNav");
                tmpArray = employmentNav.getJSONArray("results");
                employmentNav = tmpArray.getJSONObject(0);

                if (employmentNav.has("jobInfoNav")) {
                    jobInfoNav = employmentNav.getJSONObject("jobInfoNav");
                    tmpArray = jobInfoNav.getJSONArray("results");
                    jobInfoNav = tmpArray.getJSONObject(0);

                    if (jobInfoNav.has("locationNav")) {
                        locationNav = jobInfoNav.getJSONObject("locationNav");

                        if (locationNav.has("addressNavDEFLT")) {
                            addressNavDEFLT = locationNav.getJSONObject("addressNavDEFLT");
                        }
                    }
                }
            }

            // todo remove
            System.out.println("userAccountNav: " + userAccountNav.toString() + "\n");
            System.out.println("personalInfoNav: " + personalInfoNav.toString() + "\n");
            System.out.println("nativePreferredLangNav: " + nativePreferredLangNav.toString() + "\n");
            System.out.println("emailNav: " + emailNav.toString() + "\n");
            System.out.println("employmentNav: " + employmentNav.toString() + "\n");
            System.out.println("jobInfoNav: " + jobInfoNav.toString() + "\n");
            System.out.println("addressNavDEFLT: " + addressNavDEFLT.toString() + "\n");

            userMasterDataAV += "\n"
                                + userAccountNav.optString("username") + ";"
                                + personalInfoNav.optString("firstName") + ";"
                                + personalInfoNav.optString("lastName") + ";"
                                + emailNav.optString("emailAddress") + ";"
                                + personalInfoNav.optString("title") + ";"
                                + personalInfoNav.optString("gender").toLowerCase() + ";"
                                + nativePreferredLangNav.optString("externalCode") + ";"
                                + "" + ";"
                                + jobInfoNav.optString("company") + ";"
                                + addressNavDEFLT.optString("address1") + ";"
                                + addressNavDEFLT.optString("zipCode") + ";"
                                + addressNavDEFLT.optString("city") + ";"
                                + "" + ";"
                                + jobInfoNav.getNumber("jobCode").toString() + ";"
                                + ";;;;"
                                + jobInfoNav.optDouble("fte") * 100 + ";"
                                + ";;;"
                                + personalInfoNav.optString("personIdExternal ") + ";"
                                + ";"
                                + formatJSONDate(employmentNav.optString("startDate")) + ";"
                                + jobInfoNav.optString("managerId") + ";"
                                + ";;;;;;;;;;;;;"
                                + formatPayGroup(jobInfoNav.optString("payGroup")) + ";"
                                + formatCostCenter(jobInfoNav.getString("costCenter")) + ";"
                                + ";;"
                                + jobInfoNav.optString("countryOfCompany") + ";";

            // todo remove
            System.out.println("userMasterDataAV: " + userMasterDataAV + "\n");
        }

        return userMasterDataAV;
    }

    private String getParamsString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean firstRun = true;

        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (firstRun) {
                result.append("?");
                firstRun = false;
            } else {
                result.append("&");
            }

            result.append("$");
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    private String formatJSONDate(String jsonDate) {
        String date = "";
        Pattern jsonDatePattern = Pattern.compile("/Date\\((\\d+)\\)/");
        Matcher matcher = jsonDatePattern.matcher(jsonDate);

        if (matcher.matches()) {
            date = LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(matcher.group(1))),
                    ZoneId.of("Europe/Paris")).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")); // todo timezone?
        }

        return date;
    }

    private String formatPayGroup(String payGroup) {
        return payGroup.equals("D8") || payGroup.equals("D9") ? "1" : "0";
    }

    private String formatCostCenter(String sfCostCenter) {
        String[] split = sfCostCenter.split("_");

        return split[1].substring(2);
    }
}