package org.example.Trilux;

public enum Environment {
    DEVELOPMENT, STAGING, PRODUCTION;
}
