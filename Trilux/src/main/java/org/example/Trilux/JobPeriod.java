package org.example.Trilux;

public enum JobPeriod {
    HOURLY(60), DAILY(1440), WEEKLY(10080), BIWEEKLY(20160), MONTHLY(43200);

    private long minutes;

    private JobPeriod(long minutes){
        this.minutes = minutes;
    }

    public long getMinutes() {
        return minutes;
    }
}
